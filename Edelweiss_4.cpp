// Edelweiss_4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <stdlib.h>
#include <conio.h>
#include <ctime>
#include <windows.h>
using namespace std;
# define Max_Amount 11 
# define Amount_Ships 20
char table_player_0[Max_Amount][Max_Amount];
char table_player_1[Max_Amount][Max_Amount];
char table_ai_0[Max_Amount][Max_Amount];
char table_ai_1[Max_Amount][Max_Amount];
struct cell_field {
	int vertical;
	int horizontal;
	char cell;
};
cell_field mass_void_cells[(Max_Amount - 1) * (Max_Amount - 1)];
cell_field mass_4_cells[4];
int amount_void_cells = (Max_Amount - 1) * (Max_Amount - 1);
int number_of_decks = 0;
bool GoOn = true;

void create_table(char table[][Max_Amount]) {
	int i = 0, j = 0;
	char lit = 'a', num = '1';
	table[0][0] = ' ';
	for (i = 1; i < Max_Amount; i++) {
		for (j = 1; j < Max_Amount; j++) {
			table[i][j] = '-';
		}
	}
	for (i = 0, j = 1; j < Max_Amount; j++) {
		table[i][j] = lit;
		lit++;
	}
	for (j = 0, i = 1; i < Max_Amount; i++) {
		table[i][j] = num;
		num++;
	}
}

void mass_struct_inic() {
	for (int i = 1; i < Max_Amount; i++) {
		for (int j = 0; j < Max_Amount - 1; j++) {
			mass_void_cells[(i - 1) * (Max_Amount - 1) + j].vertical = i;
			mass_void_cells[(i - 1) * (Max_Amount - 1) + j].horizontal = j + 1;
			mass_void_cells[(i - 1) * (Max_Amount - 1) + j].cell = '-';
		}
	}
}

void show_table(char table_0[][Max_Amount], char table_1[][Max_Amount]) {
	for (int i = 0; i < Max_Amount; i++) {
		for (int j = 0; j < Max_Amount; j++) {
			cout << table_0[i][j] << " ";
		}
		cout << "\t";
		for (int j = 0; j < Max_Amount; j++) {
			cout << table_1[i][j] << " ";
		}
		cout << endl;
	}
}

void show_playing_field() {
	cout << "      Ваше поле:" << "\t  Поле противника:" << endl;
	show_table(table_player_0, table_player_1);
	cout << endl;
}

void show_which_ship_set(int num_of_parts) {//Данная функция носит исключительно информационный характер. Никаких проверок на правильность расстановки кораблей нет, кроме неправильно заданных координат.
	switch (num_of_parts) {
	case 1:
		cout << "Расставте свои корабли на координатной сетке." << endl;
		cout << "Выставте четыре 1-х палубных корабля" << endl << endl;
		break;
	case 2:
		cout << "Расставте свои корабли на координатной сетке." << endl;
		cout << "Выставте три 2-х палубных корабля" << endl << endl;
		break;
	case 3:
		cout << "Расставте свои корабли на координатной сетке." << endl;
		cout << "Выставте два 3-х палубных корабля" << endl << endl;
		break;
	case 4:
		cout << "Расставте свои корабли на координатной сетке." << endl;
		cout << "Выставте 4-х палубный корабль" << endl << endl;
		break;
	default:
		break;
	}
}

bool set_parts_of_ships(char table[][Max_Amount]) {
	int i = 0, j = 0;
	char lit = 0;
	system("cls");
	show_which_ship_set(number_of_decks);
	show_table(table_player_0, table_player_1);
	cout << endl;
	cout << "Задайте координаты своего корабля (составной части)." << endl;
	cout << "Сначала латинская буква, затем число:" << endl;
	cin >> lit >> i;
	if (i < 1 || i > Max_Amount) {
		cout << "Ошибка. Число задано неверно!" << endl;
		Sleep(2000);
		return set_parts_of_ships(table);
	}
	else {
		for (j = 1; j < Max_Amount; j++) {
			if (table[0][j] == lit) {
				table[i][j] = 'O';
				return false;
			}
			else {
				continue;
			}
		}
	}
	if (j == Max_Amount) {
		cout << "Ошибка. Буква задана неверно!" << endl;
		Sleep(2000);
		return set_parts_of_ships(table);
	}
	else {
		return false;
	}
}

void ship_go(int num_of_parts) {
	int count = 0;
	while (count != num_of_parts) {
		set_parts_of_ships(table_player_0);
		count++;
	}
}

void player_ship_enter(int * count_o) {
	int c = 0;
	*count_o = 4;
	ship_go(*count_o);
	while (c != 2) {
		*count_o = 3;
		ship_go(*count_o);
		c++;
	}
	c = 0;
	while (c != 3) {
		*count_o = 2;
		ship_go(*count_o);
		c++;
	}
	c = 0;
	while (c != 4) {
		*count_o = 1;
		ship_go(*count_o);
		c++;
	}
}

int who_first() {
	int menu = 0;
	system("cls");
	show_playing_field();
	cout << endl;
	cout << "----------------------------" << endl;
	cout << "Выберите пункт меню:" << endl;
	cout << "----------------------------" << endl;
	cout << "0 - Игрок ходит первым" << endl;
	cout << "1 - Компьютер ходит первым" << endl;
	cin >> menu;
	if (menu != 0 && menu != 1) {
		menu = 0;
		cout << "Ошибка. Неверно выбран пункт меню" << endl;
		Sleep(2000);
		return who_first();
	}
	else {
		return menu;
	}
}

bool get_result_hit(int turn_x) {
	if (turn_x == 0) {
		system("cls");
		show_playing_field();
		cout << "Корабль противника подбит! Вы снова ходите." << endl;
		Sleep(2000);
		return true;
	}
	else {
		system("cls");
		show_playing_field();
		cout << "Ваш корабль подбит! Компьютер снова ходит." << endl;
		Sleep(3000);
		return true;
	}
	return false;
}

void change_around_x(char table[][Max_Amount]) {
	int h = 0, l = 0, m = 0;
	for (h = 1; h < Max_Amount; h++) {
		for (l = 1; l < Max_Amount; l++) {
			if (table[h][l] == 'X') {
				if ((h - 1) > 0 && (l - 1) > 0) {
					table[h - 1][l - 1] = '*';
					for (m = 0; m < amount_void_cells; m++) {
						if (mass_void_cells[m].horizontal == (l - 1) && mass_void_cells[m].vertical == (h - 1)) {
							mass_void_cells[m].cell = '*';
							m++;
							break;
						}
					}
				}
				if ((h - 1) > 0 && table[h - 1][l] != 'X') {
					table[h - 1][l] = '*';
					for (m = 0; m < amount_void_cells; m++) {
						if (mass_void_cells[m].horizontal == l && mass_void_cells[m].vertical == (h - 1)) {
							mass_void_cells[m].cell = '*';
							m++;
							break;
						}
					}
				}
				if ((h - 1) > 0 && (l + 1) < Max_Amount) {
					table[h - 1][l + 1] = '*';
					for (m = 0; m < amount_void_cells; m++) {
						if (mass_void_cells[m].horizontal == (l + 1) && mass_void_cells[m].vertical == (h - 1)) {
							mass_void_cells[m].cell = '*';
							m++;
							break;
						}
					}
				}
				if ((l - 1) > 0 && table[h][l - 1] != 'X') {
					table[h][l - 1] = '*';
					for (m = 0; m < amount_void_cells; m++) {
						if (mass_void_cells[m].horizontal == (l - 1) && mass_void_cells[m].vertical == h) {
							mass_void_cells[m].cell = '*';
							m++;
							break;
						}
					}
				}
				if ((l + 1) < Max_Amount && table[h][l + 1] != 'X') {
					table[h][l + 1] = '*';
					for (m = 0; m < amount_void_cells; m++) {
						if (mass_void_cells[m].horizontal == (l + 1) && mass_void_cells[m].vertical == h) {
							mass_void_cells[m].cell = '*';
							m++;
							break;
						}
					}
				}
				if ((h + 1) < Max_Amount && (l - 1) > 0) {
					table[h + 1][l - 1] = '*';
					for (m = 0; m < amount_void_cells; m++) {
						if (mass_void_cells[m].horizontal == (l - 1) && mass_void_cells[m].vertical == (h + 1)) {
							mass_void_cells[m].cell = '*';
							m++;
							break;
						}
					}
				}
				if ((h + 1) < Max_Amount && table[h + 1][l] != 'X') {
					table[h + 1][l] = '*';
					for (m = 0; m < amount_void_cells; m++) {
						if (mass_void_cells[m].horizontal == l && mass_void_cells[m].vertical == (h + 1)) {
							mass_void_cells[m].cell = '*';
							m++;
							break;
						}
					}
				}
				if ((h + 1) < Max_Amount && (l + 1) < Max_Amount) {
					table[h + 1][l + 1] = '*';
					for (m = 0; m < amount_void_cells; m++) {
						if (mass_void_cells[m].horizontal == (l + 1) && mass_void_cells[m].vertical == (h + 1)) {
							mass_void_cells[m].cell = '*';
							m++;
							break;
						}
					}
				}
			}
		}
	}
}

void check_mass_void_cells(int * amount) {
	int i = 0, avc = *amount;
	for (i; i < avc; i++) {
		if ((mass_void_cells[i].cell == 'X') || (mass_void_cells[i].cell == '*')) {
			for (i; i < avc - 1; i++) {
				mass_void_cells[i] = mass_void_cells[i + 1];
			}
			avc--;
			i = 0;
		}
	}
	*amount = avc;
}

bool random_ships_set(char table_0[][Max_Amount], int parts_plus) {
	int x = 0, y = 0, r = 0, w = 0, k = 0;
	int rand_cell = 0;
	rand_cell = rand() % amount_void_cells;
	x = mass_void_cells[rand_cell].horizontal;
	y = mass_void_cells[rand_cell].vertical;
	if ((y - parts_plus) > 0 && table_0[y - parts_plus][x] == '-') {
		for (r = 0; r < amount_void_cells; r++) {
			if (mass_void_cells[r].horizontal == x && mass_void_cells[r].vertical == y - parts_plus) {
				mass_4_cells[w] = mass_void_cells[r];
				w++;
				break;
			}
			else {
				continue;
			}
		}
	}
	if ((x - parts_plus) > 0 && table_0[y][x - parts_plus] == '-') {
		for (r = 0; r < amount_void_cells; r++) {
			if (mass_void_cells[r].horizontal == x - parts_plus && mass_void_cells[r].vertical == y) {
				mass_4_cells[w] = mass_void_cells[r];
				w++;
				break;
			}
			else {
				continue;
			}
		}
	}
	if ((x + parts_plus) < Max_Amount && table_0[y][x + parts_plus] == '-') {
		for (r = 0; r < amount_void_cells; r++) {
			if (mass_void_cells[r].horizontal == x + parts_plus && mass_void_cells[r].vertical == y) {
				mass_4_cells[w] = mass_void_cells[r];
				w++;
				break;
			}
			else {
				continue;
			}
		}
	}
	if ((y + parts_plus) < Max_Amount && table_0[y + parts_plus][x] == '-') {
		for (r = 0; r < amount_void_cells; r++) {
			if (mass_void_cells[r].horizontal == x && mass_void_cells[r].vertical == y + parts_plus) {
				mass_4_cells[w] = mass_void_cells[r];
				w++;
				break;
			}
			else {
				continue;
			}
		}
	}
	if (w > 0) {
		r = rand() % w;
		if (y == mass_4_cells[r].vertical && x <= mass_4_cells[r].horizontal) {
			for (w = x; w <= x + parts_plus; w++) {
				table_0[y][w] = 'X';
				for (k = 0; k < amount_void_cells; k++) {
					if (mass_void_cells[k].horizontal == w && mass_void_cells[k].vertical == y) {
						mass_void_cells[k].cell = 'X';
						break;
					}
					else {
						continue;
					}
				}
			}
		}
		if (y == mass_4_cells[r].vertical && x > mass_4_cells[r].horizontal) {
			for (w = x; w >= x - parts_plus; w--) {
				table_0[y][w] = 'X';
				for (k = 0; k < amount_void_cells; k++) {
					if (mass_void_cells[k].horizontal == w && mass_void_cells[k].vertical == y) {
						mass_void_cells[k].cell = 'X';
						break;
					}
					else {
						continue;
					}
				}
			}
		}
		if (x == mass_4_cells[r].horizontal && y < mass_4_cells[r].vertical) {
			for (w = y; w <= y + parts_plus; w++) {
				table_0[w][x] = 'X';
				for (k = 0; k < amount_void_cells; k++) {
					if (mass_void_cells[k].horizontal == x && mass_void_cells[k].vertical == w) {
						mass_void_cells[k].cell = 'X';
						break;
					}
					else {
						continue;
					}
				}
			}
		}
		if (x == mass_4_cells[r].horizontal && y > mass_4_cells[r].vertical) {
			for (w = y; w >= y - parts_plus; w--) {
				table_0[w][x] = 'X';
				for (k = 0; k < amount_void_cells; k++) {
					if (mass_void_cells[k].horizontal == x && mass_void_cells[k].vertical == w) {
						mass_void_cells[k].cell = 'X';
						break;
					}
					else {
						continue;
					}
				}
			}
		}
		change_around_x(table_0);
		check_mass_void_cells(&amount_void_cells);
		return false;
	}
	else {
		return random_ships_set(table_0, parts_plus);
	}
}

void ships_set(char table_0[][Max_Amount], int *amount) {
	int nod = 3;
	random_ships_set(table_0, nod);
	nod--;
	random_ships_set(table_0, nod);
	random_ships_set(table_0, nod);
	nod--;
	random_ships_set(table_0, nod);
	random_ships_set(table_0, nod);
	random_ships_set(table_0, nod);
	nod--;
	random_ships_set(table_0, nod);
	random_ships_set(table_0, nod);
	random_ships_set(table_0, nod);
	random_ships_set(table_0, nod);
	for (int i = 1; i < Max_Amount; i++) {
		for (int j = 1; j < Max_Amount; j++) {
			if (table_0[i][j] == 'X') {
				table_0[i][j] = 'O';
			}
			if (table_0[i][j] == '*') {
				table_0[i][j] = '-';
			}
		}
	}
	*amount = (Max_Amount - 1) * (Max_Amount - 1);
}

bool fill_player_table(char table[][Max_Amount]) {
	int menu = 0;
	system("cls");
	show_playing_field();
	cout << endl;
	cout << "----------------------------" << endl;
	cout << "Выберите пункт меню:" << endl;
	cout << "----------------------------" << endl;
	cout << "0 - Расставить свои корабли вручную." << endl;
	cout << "1 - Расставить свои корабли автоматически случайно." << endl;
	cout << "2 - Играть." << endl;
	cin >> menu;
	switch (menu) {
	case 0:
		player_ship_enter(&number_of_decks);
		return fill_player_table(table);
		break;
	case 1:
		ships_set(table_player_0, &amount_void_cells);
		return false;
	case 2:
		return false;
		break;
	default:
		menu = 0;
		cout << "Ошибка. Неверно выбран пункт меню!" << endl;
		Sleep(2000);
		return fill_player_table(table);
		break;
	}
}

bool hit_check(int i, int j, char table_0[][Max_Amount], char table_1[][Max_Amount], int turn_x) {
	int k = 0;
	cout << table_0[0][j] << i << endl;
	Sleep(4000);
	if (table_0[i][j] == 'O') {
		table_0[i][j] = 'Y';
		table_1[i][j] = 'Y';
		for (k = j; k < Max_Amount - 1; k++) {
			if (table_0[i][k] == 'Y' && table_0[i][k + 1] == 'O') {
				return get_result_hit(turn_x);
			}
			else {
				continue;
			}
		}
		for (k = j; k > 1; k--) {
			if (table_0[i][k] == 'Y' && table_0[i][k - 1] == 'O') {
				return get_result_hit(turn_x);
			}
			else {
				continue;
			}
		}
		for (k = i; k < Max_Amount - 1; k++) {
			if (table_0[k][j] == 'Y' && table_0[k + 1][j] == 'O') {
				return get_result_hit(turn_x);
			}
			else {
				continue;
			}
		}
		for (k = i; k > 1; k--) {
			if (table_0[k][j] == 'Y' && table_0[k - 1][j] == 'O') {
				return get_result_hit(turn_x);
			}
			else {
				continue;
			}
		}
		for (i = 1; i < Max_Amount; i++) {
			for (j = 1; j < Max_Amount; j++) {
				if (table_0[i][j] == 'Y') {
					if (turn_x == 1) {
						table_0[i][j] = 'X';
						table_1[i][j] = 'X';
						for (k = 0; k < amount_void_cells; k++) {
							if (mass_void_cells[k].horizontal == j && mass_void_cells[k].vertical == i) {
								mass_void_cells[k].cell = 'X';
								break;
							}
							else {
								continue;
							}
						}
					}
					else {
						table_0[i][j] = 'X';
						table_1[i][j] = 'X';
					}
				}
			}
		}
		system("cls");
		show_playing_field();
		switch (turn_x) {
		case 0:
			system("cls");
			show_playing_field();
			cout << "Корабль противника потоплен! Вы снова ходите." << endl;
			Sleep(2000);
			return true;
			break;
		case 1:
			change_around_x(table_ai_1);
			check_mass_void_cells(&amount_void_cells);
			system("cls");
			show_playing_field();
			cout << "Ваш корабль потоплен! Компьютер снова ходит." << endl;
			Sleep(3000);
			return true;
			break;
		}
	}
	else {
		if (table_0[i][j] == '*' || table_0[i][j] == 'X' || table_0[i][j] == 'Y') {
			system("cls");
			show_playing_field();
			cout << "Координаты заданы неверно! Ходите еще раз.";
			Sleep(2000);
			return true;
		}
		else {
			table_0[i][j] = '*';
			table_1[i][j] = '*';
			switch (turn_x) {
			case 0:
				system("cls");
				show_playing_field();
				cout << "Промах! Следующий ход компьютера." << endl;
				Sleep(2000);
				return false;
				break;
			case 1:
				for (k = 0; k < amount_void_cells; k++) {
					if (mass_void_cells[k].horizontal == j && mass_void_cells[k].vertical == i) {
						mass_void_cells[k].cell = '*';
						break;
					}
					else {
						continue;
					}
				}
				check_mass_void_cells(&amount_void_cells);
				system("cls");
				show_playing_field();
				cout << "Компьютер промахнулся! Вы ходите." << endl;
				Sleep(3000);
				return false;
				break;
			default:
				break;
			}
		}
	}
	return false;
}

int win_check(char table[][Max_Amount], int whose_turn) {
	int count_ships = 0;
	for (int i = 1; i < Max_Amount; i++) {
		for (int j = 1; j < Max_Amount; j++) {
			if (table[i][j] == 'X') {
				count_ships++;
			}
			else {
				continue;
			}
		}
	}
	if (count_ships == Amount_Ships && whose_turn == 0) {
		return 0;
	}
	else {
		if (count_ships == Amount_Ships && whose_turn == 1) {
			return 1;
		}
		else {
			return 2;
		}
	}
}

bool result_win_check(bool * GoOn, int turn_result) {
	switch (turn_result) {
	case 0:
		system("cls");
		show_playing_field();
		cout << "Вы победили. Поздравляем!" << endl;
		*GoOn = false;
		return false;
		break;
	case 1:
		system("cls");
		show_playing_field();
		cout << "Победа компьютера." << endl;
		*GoOn = false;
		return false;
		break;
	case 2:
		*GoOn = true;
		return true;
		break;
	default:
		cout << "Ошибка приложения" << endl;
		return false;
		break;
	}
}

bool ai_turn(int whose_turn) {
	int w = 0, x = 0, y = 0, z = 0, turn_result = 0;
	turn_result = win_check(table_ai_1, whose_turn);
	if (!result_win_check(&GoOn, turn_result)) {
		return false;
	}
	else {
		for (y = 1; y < Max_Amount; y++) {
			for (x = 1; x < Max_Amount; x++) {
				if (table_ai_1[y][x] == 'Y') {
					if (table_ai_1[y][x + 1] == 'Y') {
						for (z = x; table_ai_1[y][z] != '*' || z < Max_Amount; z++) {
							if (table_ai_1[y][z] == '-') {
								if (table_ai_1[y][x - 1] == '-') {
									w = rand() % 2;
									if (w == 0) {
										if (hit_check(y, x - 1, table_player_0, table_ai_1, whose_turn)) {
											return ai_turn(whose_turn);
										}
										else {
											return false;
										}
									}
									else {
										if (hit_check(y, z, table_player_0, table_ai_1, whose_turn)) {
											return ai_turn(whose_turn);
										}
										else {
											return false;
										}
									}
								}
								else {
									if (hit_check(y, z, table_player_0, table_ai_1, whose_turn)) {
										return ai_turn(whose_turn);
									}
									else {
										return false;
									}
								}
							}
							else {
								continue;
							}
						}
						if (hit_check(y, x - 1, table_player_0, table_ai_1, whose_turn)) {
							return ai_turn(whose_turn);
						}
						else {
							return false;
						}
					}
					else {
						if (table_ai_1[y + 1][x] == 'Y') {
							for (z = y; table_ai_1[z][x] != '*' || z < Max_Amount; z++) {
								if (table_ai_1[z][x] == '-') {
									if (table_ai_1[y - 1][x] == '-') {
										w = rand() % 2;
										if (w == 0) {
											if (hit_check(y - 1, x, table_player_0, table_ai_1, whose_turn)) {
												return ai_turn(whose_turn);
											}
											else {
												return false;
											}
										}
										else {
											if (hit_check(z, x, table_player_0, table_ai_1, whose_turn)) {
												return ai_turn(whose_turn);
											}
											else {
												return false;
											}
										}
									}
									else {
										if (hit_check(z, x, table_player_0, table_ai_1, whose_turn)) {
											return ai_turn(whose_turn);
										}
										else {
											return false;
										}
									}
								}
								else {
									continue;
								}
							}
							if (hit_check(y - 1, x, table_player_0, table_ai_1, whose_turn)) {
								return ai_turn(whose_turn);
							}
							else {
								return false;
							}
						}
						else {
							w = 0;
							for (z = 0; z < amount_void_cells; z++) {
								if (mass_void_cells[z].horizontal == x + 1 && mass_void_cells[z].vertical == y) {
									mass_4_cells[w] = mass_void_cells[z];
									w++;
								}
								else {
									if (mass_void_cells[z].horizontal == x && mass_void_cells[z].vertical == y + 1) {
										mass_4_cells[w] = mass_void_cells[z];
										w++;
									}
									else {
										if (mass_void_cells[z].horizontal == x - 1 && mass_void_cells[z].vertical == y) {
											mass_4_cells[w] = mass_void_cells[z];
											w++;
										}
										else {
											if (mass_void_cells[z].horizontal == x && mass_void_cells[z].vertical == y - 1) {
												mass_4_cells[w] = mass_void_cells[z];
												w++;
											}
											else {
												continue;
											}
										}
									}
								}
							}
							z = rand() % w;
							x = mass_4_cells[z].horizontal;
							y = mass_4_cells[z].vertical;
							if (hit_check(y, x, table_player_0, table_ai_1, whose_turn)) {
								return ai_turn(whose_turn);
							}
							else {
								return false;
							}
						}
					}
				}
			}
		}
		int rand_cell = 0;
		rand_cell = rand() % amount_void_cells;
		x = mass_void_cells[rand_cell].horizontal;
		y = mass_void_cells[rand_cell].vertical;
		if (hit_check(y, x, table_player_0, table_ai_1, whose_turn)) {
			return ai_turn(whose_turn);
		}
		else {
			return false;
		}
	}
}

bool player_turn(int whose_turn) {
	int x = 0, y = 0, turn_result = 0;
	char lit = 0;
	turn_result = win_check(table_player_1, whose_turn);
	if (!result_win_check(&GoOn, turn_result)) {
		return false;
	}
	else {
		system("cls");
		show_playing_field();
		cout << endl;
		cout << "Ваш ход. Задайте координаты удара:" << endl;
		cout << "Сначала латинская буква, затем число:" << endl;
		cin >> lit >> y;
		if (y < 1 || y > Max_Amount) {
			lit = 0;
			y = 0;
			cout << "Ошибка. Число задано неверно!" << endl;
			Sleep(2000);
			return player_turn(whose_turn);
		}
		else {
			for (x = 1; x < Max_Amount; x++) {
				if (table_ai_0[0][x] == lit) {
					if (hit_check(y, x, table_ai_0, table_player_1, whose_turn)) {
						return player_turn(whose_turn);
					}
					else {
						return false;
					}
				}
				else {
					continue;
				}
			}
		}
		if (y == Max_Amount) {
			lit = 0;
			y = 0;
			cout << "Ошибка. Буква задана неверно!" << endl;
			Sleep(2000);
			return player_turn(whose_turn);
		}
		else {
			return false;
		}
	}
}

int main(int argc, char* argv[]) {
	setlocale(LC_ALL, "Rus");
	int whose_turn = 0;
	srand(time_t(NULL));
	create_table(table_player_0);
	create_table(table_player_1);
	create_table(table_ai_0);
	create_table(table_ai_1);
	mass_struct_inic();
	fill_player_table(table_player_0);
	whose_turn = who_first();
	mass_struct_inic();
	ships_set(table_ai_0, &amount_void_cells);
	mass_struct_inic();
	while (GoOn) {
		switch (whose_turn) {
		case 0:
			player_turn(whose_turn);
			whose_turn = 1;
			break;
		case 1:
			ai_turn(whose_turn);
			whose_turn = 0;
			break;
		default:
			cout << "Ошибка приложения. Запустите программу еще раз." << endl;
			return 0;
			break;
		}
	}
	system("pause");
	return 0;
}